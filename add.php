<?php
    include 'includes/header.php';
?>
<body>
<h3 class="text-center">Add</h3>
    <div class="container">
        <div class="row">
            <div class="col-lg-6 offset-3">
            
            <form action="includes/add-inc.php" method="post">

                <div class="form-group">
                    <label for="fullname">Fullname</label>
                    <input type="text" class="form-control" name="fullname">
                </div>

                <div class="form-group">
                    <label for="email">Email</label>
                    <input type="email" class="form-control" name="email">
                </div>

                <div class="form-group">
                    <label for="job">Job</label>
                    <input type="text" class="form-control" name="job">
                </div>

                <div class="form-group">
                    <label for="address">Address</label>
                    <input type="text" class="form-control" name="address">
                </div>

                <div class="form-group">
                    <label for="company">Company</label>
                    <input type="text" class="form-control" name="company">
                </div>

                <button type="submit" name="submit" class="btn btn-success m-2">Submit</button>

            </form>
            </div>
        </div>
    </div>



<?php
    include 'includes/footer.php';
?>
</body>
</html>