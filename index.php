<?php
    include 'includes/header.php';
?>
<body>
<h3 class="text-center mt-2">Tables</h3>
<form action="search.php" method="POST">
<div class="d-flex">
    <input type="text" name="search" class="form-control col-md-6 offset-3 mb-2">

</div>

</form>
<div class="container">
    <a href="add.php"  class="btn btn-success mb-2 mt-2"> Add new Task</a>
    <button class="btn btn-primary pull-right">Print</button>
    <div class="straigh-bar mb-3" style="border:2px solid gray;"></div>
    <table class="table table-bordered table-hover table-striped">
        <thead>
            <tr>
                <th>ID</th>
                <th>Fullname</th>
                <th>Email</th>
                <th>Job</th>
                <th>Address</th>
                <th>Company</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
            <tr> 
            <?php
                $page = (isset($_GET['page']) ? $_GET['page'] : 1);
                $perPage = (isset($_GET['per-page']) && ($_GET['per-page']) <= 50 ? $_GET['per-page']:10);
                $start = ($page > 1) ? ($page * $perPage) - $perPage : 0;

                $sql = "SELECT * FROM employees limit ".$start." , ".$perPage." ";
                $total = $conn->query("Select * from employees")->num_rows;
                $pages = ceil($total / $perPage);
                /* echo $total; */
                $result = mysqli_query($conn, $sql);
                $rowCount = mysqli_num_rows($result);
                if($rowCount >0){
                    while($row = mysqli_fetch_assoc($result)) :
                        ?>
                        <td>
                            <?php echo  $row['id'];  ?>
                        </td>
                        <td>
                            <?php echo  $row['fullname'];  ?>
                        </td>
                        <td>
                            <?php echo  $row['email'];  ?>
                        </td>
                        <td>
                            <?php echo  $row['job'];  ?>
                        </td>
                        <td>
                            <?php echo  $row['address'];  ?>
                        </td>
                        <td>
                            <?php echo  $row['company'];  ?>
                        </td>
                
                <td>
                <div class="d-flex">
                    <a class="btn btn-warning m-1" href="update.php?id=<?php echo $row['id'];?>" name="submit">Edit</button>
                    <a href="delete.php?id=<?php echo $row['id'];?>" class="btn btn-danger m-1">
                        Delete                        
                    </a>
                </div>
                </td>
                 </tr>
                    <?php endwhile; ?>
                   
                        <?php
                } else{
                    echo "No results found";
                }

            ?>         
        </tbody>
        <tfoot>
            <tr>
                <th>ID</th>
                <th>Fullname</th>
                <th>Email</th>
                <th>Job</th>
                <th>Address</th>
                <th>Company</th>
                <th>Actions</th>
            </tr>
        </tfoot>
    </table>
        <ul class="pagination justify-content-center"> 
        <?php
            for ($i=1; $i <= $pages ; $i++) : ?>
            <li class="page-item">
                <a class="page-link" href="?page=<?php echo $i; ?>"><?php echo $i; ?></a>
            </li>
            <?php endfor; ?>
        </ul>
</div>



<?php
    include 'includes/footer.php';
?>
</body>
</html>