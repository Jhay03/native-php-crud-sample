<?php
//params to connect to the database.
$dbHost = "localhost";
$dbUser = "root";
$dbPass = "";
$dbName = "native_php";


//connection to database
// need 4 params to connect
$conn = mysqli_connect($dbHost, $dbUser, $dbPass, $dbName);


if(!$conn){
    die("Database connection failed!");
}

?>