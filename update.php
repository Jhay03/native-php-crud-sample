<?php
    include 'includes/header.php';
    
?>
<?php 

include 'includes/db.php';
$id = $_GET['id'];

$sql = "SELECT * FROM employees where id = '$id'";

$rows = $conn->query($sql);

$row = $rows->fetch_assoc();

if(isset($_POST['submit'])){
    $fullname = $_POST['fullname'];
    $email = $_POST['email'];
    $job = $_POST['job'];
    $address = $_POST['address'];
    $company = $_POST['company'];
    $sql2 = "UPDATE employees set fullname = '$fullname', email = '$email', job = '$job', address = '$address', company = '$company' where id = '$id'";
    $conn->query($sql2);
    header("Location: index.php?=successfullyupdated!");
}

?>

<body>
<h3 class="text-center">Edit</h3>
    <div class="container">
        <div class="row">
            <div class="col-lg-6 offset-3">
            
            <form method="post">

                <div class="form-group">
                    <label for="fullname">Fullname</label>
                    <input type="text" class="form-control" name="fullname" value=" <?php echo  $row['fullname'];  ?>">
                </div>

                <div class="form-group">
                    <label for="email">Email</label>
                    <input type="email" class="form-control" name="email" value="<?php echo $row['email'];?>">
                </div>

                <div class="form-group">
                    <label for="job">Job</label>
                    <input type="text" class="form-control" name="job" value="<?php echo $row['job'];?>">
                </div>

                <div class="form-group">
                    <label for="address">Address</label>
                    <input type="text" class="form-control" name="address" value="<?php echo $row['address'];?>">
                </div>

                <div class="form-group">
                    <label for="company">Company</label>
                    <input type="text" class="form-control" name="company" value="<?php echo $row['company'];?>">
                </div>

                <button type="submit" name="submit" class="btn btn-primary m-2">Update</button>

            </form>
            </div>
        </div>
    </div>



<?php
    include 'includes/footer.php';
?>
</body>
</html>