<?php
    include 'includes/header.php';
    if(isset($_POST['search'])){
        $search = $_POST['search'];
        $sql = "SELECT * FROM employees where fullname like '%$search%'";
        $rows = $conn->query($sql);
        }
?>
<body>
<h3 class="text-center mt-2">Tables</h3>
<form action="index.php" method="POST">
<div class="d-flex">
    <input type="text" name="search" class="form-control col-md-6 offset-3 mb-2">

</div>

</form>
<div class="container">
    <a href="add.php"  class="btn btn-success mb-2 mt-2"> Add new Task</a>
    <button class="btn btn-primary pull-right">Print</button>
    <div class="straigh-bar mb-3" style="border:2px solid gray;"></div>
    <table class="table table-bordered table-hover table-striped">
        <thead>
            <tr>
                <th>ID</th>
                <th>Fullname</th>
                <th>Email</th>
                <th>Job</th>
                <th>Address</th>
                <th>Company</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
            <tr> 
            <?php
              
                $result = mysqli_query($conn, $sql);
                $rowCount = mysqli_num_rows($result);
                if($rowCount >0){
                    while($row = mysqli_fetch_assoc($result)) :
                        ?>
                        <td>
                            <?php echo  $row['id'];  ?>
                        </td>
                        <td>
                            <?php echo  $row['fullname'];  ?>
                        </td>
                        <td>
                            <?php echo  $row['email'];  ?>
                        </td>
                        <td>
                            <?php echo  $row['job'];  ?>
                        </td>
                        <td>
                            <?php echo  $row['address'];  ?>
                        </td>
                        <td>
                            <?php echo  $row['company'];  ?>
                        </td>
                
                <td>
                <div class="d-flex">
                    <a class="btn btn-warning m-1" href="update.php?id=<?php echo $row['id'];?>" name="submit">Edit</button>
                    <a href="delete.php?id=<?php echo $row['id'];?>" class="btn btn-danger m-1">
                        Delete                        
                    </a>
                </div>
                </td>
                 </tr>
                    <?php endwhile; ?>
                   
                        <?php
                } else{
                    ?>
                    <tr>
                    <td>
                    <?php
                    echo "<h1>No results found</h1>";
                    ?>
                 
                    </td>
                    <td>
                    <a href="index.php" class="btn btn-success"> BACK </a>
                    </td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    </tr>
                    <?php
                }

            ?>         
        </tbody>
        <tfoot>
            <tr>
                <th>ID</th>
                <th>Fullname</th>
                <th>Email</th>
                <th>Job</th>
                <th>Address</th>
                <th>Company</th>
                <th>Actions</th>
            </tr>
        </tfoot>
    </table>
</div>



<?php
    include 'includes/footer.php';
?>
</body>
</html>